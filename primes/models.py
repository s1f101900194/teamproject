# よく読んでね ===> https://qiita.com/kumao03/items/45522656fd5a12ebbfd1

from django.db import models
#from django.db import Max,Min,Avg

class Player(models.Model):
    """プレイヤー情報"""
    name = models.CharField(max_length=64)
    max = models.IntegerField(default=0)
    times = models.IntegerField(default=0)
    ave = models.IntegerField(default=0)

class Game(models.Model):
    """ゲーム情報"""
    figure = models.IntegerField(default=-1)
    points = models.IntegerField(default=0)
    life = models.IntegerField(default=3)
    log = models.TextField()
    difficulty = models.IntegerField(default=-1, choices=((0, "easy"), (1, "normal"), (2, "hard"), (3, "lunatic")))
    player = models.ForeignKey(Player, related_name='player', on_delete=models.CASCADE)
    info = models.TextField()
    