"""config URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path(r'admin/', admin.site.urls),
    path(r'', views.primes, name='primes'),
    path(r'nickname', views.nickname, name='nickname'),
    path(r'difficulty/<int:game_id>/', views.difficulty, name='difficulty'),
    path(r'difficulty/<int:game_id>/easy', views.easy, name='easy'),
    path(r'difficulty/<int:game_id>/normal', views.normal, name='normal'),
    path(r'difficulty/<int:game_id>/hard', views.hard, name='hard'),
    path(r'game/<int:game_id>/', views.game, name='game'),
    path(r'game/<int:game_id>/yes', views.yes, name='yes'),
    path(r'game/<int:game_id>/no', views.no, name='no'),
    path(r'game/<int:game_id>/skip', views.skip, name='skip'),
    path(r'game/<int:game_id>/result', views.result, name='result'),
    path(r'game/<int:game_id>/again', views.again, name='again'),
    path(r'record', views.record, name='record'),
    path(r'record/player/<int:player_id>', views.player, name='player'),
    path(r'record/player/unknown', views.unknown, name='unknown'),
    path(r'record/gamelog/<int:game_id>', views.gamelog, name='gamelog'),
    path(r'record/unknown',views.unknown,name='unknown'),
]
