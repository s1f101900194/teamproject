from django.shortcuts import render, redirect
from .models import Game, Player
import math
import time
import random

# Create your views here.


def checkPrime(n):      #素数判定
    for i in range(2, int(math.sqrt(n))+2):
        if n % i == 0:
            return False
    return True

def figureGenerator(difficulty, points) :      #点数の計算
    if difficulty == 0 :        #easyなら
        return int(random.uniform(3, 81))
    elif difficulty == 1 :      #normalなら
        while 1 :
            num = int(random.uniform(2, 10 + points * 0.2))*2-1
            if num < 200 :
                return num

    elif difficulty == 2 :      #hardなら
        while 1 :
            num = int(random.uniform(10, 20 + points * 0.3))*2-1
            if num < 10**11 :
                return num
        
def correct(points) :       #正解時の表示の処理
    info = "Correct    + " + str(points)
    if math.log2(points + 1) - int(math.log2(points + 1)) == 0 :       #メルセンヌ素数なら
        points *= 3
        info += " × 3 (Mersenne)"
    if math.log2(math.log2(points - 1)) - int(math.log2(math.log2(points - 1))) == 0 :       #フェルマー素数なら
        points *= 4
        info += " × 4 (Fermat)"
    if points > 40 :     #変数kaiが虚数にならないようにする
        kai = (-1 + math.sqrt(4 * points - 163)) / 2     #解の方程式
        if  kai - int(kai) == 0 :    #オイラー素数なら   
            points *= 5
            info += " × 5 (Euker)"
    return points, info

def primes(request):        #ホーム画面
    time.sleep(0.5)
    return render(request, 'primes.html')

def nickname(request) :     #ニックネーム入力画面
    if request.method == 'POST':        #formのNEXTボタンが押されたら
        player = Player.objects.filter(name = request.POST['name'])
        if len(request.POST['name']) == 0 or len(request.POST['name']) > 64 :   #formに何も書かれていなかったり、65文字以上入力されていたら
                return render(request, 'nickname.html')
        if player.count() == 0 : #もしもnicknameがデータベース内になかったら
            nickname = Player.objects.create(name = request.POST['name'])
            playerId = nickname.id
            newGame = Game.objects.create(player_id = playerId)
        else:       #もしもnicknameがデータベース内にあったら
            playerId = player[0].id
            newGame = Game.objects.create(player_id = playerId)
        time.sleep(0.5)
        return redirect('difficulty', newGame.id)
    time.sleep(0.5)
    return render(request, 'nickname.html')

def difficulty(request, game_id) :      #難易度設定画面
    game = {'id' : Game.objects.get(pk = game_id).id}
    time.sleep(0.5)
    return render(request, 'difficulty.html', game)

def easy(request, game_id) :        #難易度easyの処理
    newGame = Game.objects.get(pk = game_id)
    newGame.difficulty = 0
    newGame.save()
    return redirect('game', game_id)
    
def normal(request, game_id) :      #難易度mormalの処理
    game = Game.objects.get(pk = game_id)
    game.difficulty = 1
    game.save()
    return redirect('game', game_id)

def hard(request, game_id) :        #難易度hardの処理
    game = Game.objects.get(pk = game_id)
    game.difficulty = 2
    game.save()
    return redirect('game', game_id)

def game(request, game_id) :        #ゲーム画面
    game = Game.objects.get(pk = game_id)
    game.figure = figureGenerator(game.difficulty, game.points)     #乱数の生成
    game.save()
    life = "♥" * Game.objects.get(pk = game_id).life       #画面下のlife：の表示のい処理
    if life == "" :     #lifeが0になったら
        player = Game.objects.get(pk = game_id).player
        player.times += 1       #プレイヤーのゲーム回数を+1
        player.save()
        if player.ave == 0 :        #プレイヤーが初めてゲームをしたら
            player.ave = game.points        #pointを平均にする
        else :      #プレイヤーが2回以上ゲームをしていたん場合
            player.ave = (player.ave * (player.times - 1)  + game.points) / player.times    #平均の計算
        if game.points > player.max :       #プレイヤーの最高得点の処理
            player.max = game.points
        player.save()
        return redirect('result', game_id)
    data = {
        'id' : Game.objects.get(pk = game_id).id,
        'figure' : Game.objects.get(pk = game_id).figure,
        'points' : Game.objects.get(pk = game_id).points,
        'info' : Game.objects.get(pk = game_id).info,
        'life' : life,
        }
    return render(request, 'game.html', data)

def yes(request, game_id) :     #yesボタンが押された時の処理
    game = Game.objects.get(pk = game_id)
    if checkPrime(game.figure) :        #合っていたら
        points, info = correct(game.figure)
        game.points += points
        game.log += str(game.figure) + ","
        game.info = info
        game.save()
    else :      #間違っていたら
        game.life -= 1
        game.info = "Incorrect"
        game.save()
    return redirect('game', game_id)

def no(request, game_id) :      #noボタンが押された時の処理
    game = Game.objects.get(pk = game_id)
    if not checkPrime(game.figure) :        #合っていたら
        game.points += game.figure
        game.log += str(game.figure) + ","
        game.info = "Correct    + " + str(game.figure)
        game.save()
    else :      #間違っていたら
        game.life -= 1
        game.info = "Incorrect"
        game.save()
    return redirect('game', game_id)

def skip(request, game_id) :        #passボタンが押された時の処理
    if random.random() > 0.5 :      #50%の確率で
        game = Game.objects.get(pk = game_id)
        game.life -= 1
        game.save()
        game.info = "Skip"
    return redirect('game', game_id)

def result(request, game_id) :      #リザルト画面
    line = ""
    lineResult = []
    log = Game.objects.get(pk = game_id).log
    logList = log.split(",")
    for i in logList[:-1] :     #logListの数字を1つ1つ取り出す
        line += str(i) + " "
        if len(line) > 60 :     #60文字以上で文字を折り返す
            lineResult += [line]
            line = ""
    lineResult += [line]
    data = {
        'id' : Game.objects.get(pk = game_id).id,
        'log' : lineResult,
        'points' : Game.objects.get(pk = game_id).points,
        }
    return render(request, 'result.html', data)

def again(request, game_id) :       #もう一度ゲームをする場合
    diff = Game.objects.get(pk = game_id).difficulty        #前回のゲームと同じ難易度
    playerId = Game.objects.get(pk = game_id).player.id     #前回のゲームと同じプレイヤーで
    newGame = Game.objects.create(difficulty = diff, player_id = playerId)      #新しくゲームを作る
    return redirect('game', newGame.id)

def record(request) :       #レコード画面
    time.sleep(0.5)
    if request.method == 'POST':        #検索されたら
        for i in Player.objects.all():      #プレイヤーを一人づつ取り出す
            if request.POST['nickname'] == i.name:
                return redirect ('player', i.id)
            return redirect('unknown')    

    games = Game.objects.all()
    if ('sort' in request.GET):     #ソートのリンクを踏んだら
        if (request.GET['sort']=='id'):
            games = Game.objects.order_by('-id')
        else:
            games = Game.objects.order_by('-points')
    else:
        games = Game.objects.order_by('-points')  
    
    editedGame = []
    for i in games :
        if i.life <= 0 :    #もしゲームが終了していたら
            editedGame += [i]
            if len(editedGame) > 100 :      #100件以上は非表示化
                break
    data = {
        "games" : editedGame
    }
    return render(request, 'record.html', data)
    
def player(request, player_id) :        #プレイヤー画面
    time.sleep(0.5)
    gameList = []

    if ('sort' in request.GET):     #ソートのリンクを踏んだら  
        if (request.GET['sort']=='id'):
            games = Game.objects.order_by('-id')
        else:
            games = Game.objects.order_by('-points')
    else:
        games = Game.objects.order_by('-points')  

    for game in games :        
        if game.player.id == player_id :    #指定したプレイヤーのみのゲームログを抜き出す
            if game.life  <= 0 :        #もしゲームが終了していたら
                gameList += [game]
                if len(gameList) > 100 :      #100件以上は非表示化
                    break

    data = {
        "id" : Player.objects.get(pk = player_id).id,
        "name" : Player.objects.get(pk = player_id).name,
        "max" : Player.objects.get(pk = player_id).max,
        "times" : Player.objects.get(pk = player_id).times,
        "ave" : Player.objects.get(pk = player_id).ave,
        "games" : gameList,
    }

    return render(request, 'player.html', data)

def gamelog(request, game_id) :     #ゲームログ画面
    time.sleep(0.5)
    line = ""
    lineResult = []
    log = Game.objects.get(pk = game_id).log
    logList = log.split(",")
    for i in logList[:-1] :     #logListの数字を1つ1つ取り出す
        line += str(i) + " "
        if len(line) > 70 :     #70文字以上で文字を折り返す
            lineResult += [line]
            line = ""
    lineResult += [line]

    diffi = Game.objects.get(pk=game_id).difficulty     # {{ data.get_difficulty_display }} が使えなかったので条件で取り出しました。
    if diffi == 0 :
        strDiffi = "easy"
    elif diffi == 1 :
        strDiffi = "normal"
    else :
        strDiffi = "hard"

    data = {
        'name' : Game.objects.get(pk = game_id).player.name,
        'nameId' : Game.objects.get(pk = game_id).player.id,
        'difficulty' : strDiffi,
        'points' : Game.objects.get(pk = game_id).points,
        'log' : lineResult,
        }
    return render(request, 'gamelog.html', data)  

def unknown(request) :      #ユーザーが見つかりませんでした画面
    return render(request, 'unknown.html')
